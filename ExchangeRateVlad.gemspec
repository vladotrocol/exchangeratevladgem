Gem::Specification.new do |s|
  s.name        = 'ExchangeRateVlad'
  s.version     = '0.0.7'
  s.date        = '2015-06-01'
  s.summary     = "Currency exchange rate"
  s.description = "A gem for FreeAgent"
  s.authors     = ["Vlad Otrocol"]
  s.email       = 'vladotrocol@gmail.com'
  s.files       = ["lib/ExchangeRateVlad.rb"]
  s.homepage    =
    'https://bitbucket.org/vladotrocol/exchangeratevladgem'
  s.license       = 'MIT'
end