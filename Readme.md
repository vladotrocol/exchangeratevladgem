# Exchange Rate for FreeAgent
## Usage
### Ruby

install the local gem by doing:

`gem install ExchangeRateVlad-0.0.7.gem`

or install the remote one by doing:

`gem install ExchangerateVlad -v 0.0.7`

to test it try:

`require 'ExchangeRateVlad'`

`ExchangeRate.init("http://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml")`

`p ExchangeRate.at(Date.today, 'GBP', 'RON')`

It also works with a local file:

`require 'ExchangeRateVlad'`

`ExchangeRate.init("http://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml")`

`p ExchangeRate.at(Date.today, 'EUR', 'RON')`

`p ExchangeRate.get_currencies()`

`p ExchangeRate.get_times()`


### Rails 
 make sure you add the gem in your Gemfile:

 `gem 'ExchangeRateeVlad', '0.0.7'`



