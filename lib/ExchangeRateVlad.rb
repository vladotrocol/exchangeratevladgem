class Converter
    require 'nokogiri'
    @@doc
    @@currencies = ['EUR']
    @@times = []
    def init(file)
    	if file.start_with?('http://') || file.start_with?('https://')
    		require 'open-uri'
    		begin 
				@@doc = Nokogiri::XML(open(file))
    		rescue
    			p "Unable to open location"
    			return false
    		end
    	else
    		begin
    			f = File.open(file)
				@@doc = Nokogiri::XML(f)
				f.close
			rescue
				p 'Unable to open Location'
				return false
			end
		end
		return true
    end
    def get_currencies()
    	c = @@doc.css('Cube Cube').first.css('Cube')
    	for x in c
    		@@currencies.push(x['currency'])
    	end
    	return @@currencies
    end
    def get_times()
    	c = @@doc.css('Cube Cube[@time]')
    	for x in c
    		@@times.push(x['time'])
    	end
    	return @@times
    end
	def at(date, c1, c2)
		d = date.strftime("%Y-%m-%d")
		r = @@doc.css('Cube Cube[@time="'+d+'"]')
		if r.size==0
			p 'No data for the specified time of exchange rate: ' + d
			return -1
		end
		begin
			if c1 == 'EUR'
				er1 = 1.0
			else
				er1 = r.at_css('[@currency="'+c1+'"]')['rate'].to_f
			end
		rescue
			p 'Something is wrong with this currency: ' + c1
			return -1
		end
		begin
			if c2 == 'EUR'
				er2 = 1.0
			else
				er2 = r.at_css('[@currency="'+c2+'"]')['rate'].to_f
			end
		rescue
			p 'Something is wrong with this currency: ' + c2
			return -1
		end
		return er2/er1
	end
end

require 'date'

ExchangeRate = Converter.new